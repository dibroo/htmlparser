using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Argos.HtmlParser
{

    public interface ILexer<TLexem> : ITranslator<char, TLexem> { }

}