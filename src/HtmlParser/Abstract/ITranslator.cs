using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Argos.HtmlParser
{
    public interface ITranslator<TFrom, TTo>
    {
        IEnumerable<TTo> Translate(IEnumerable<TFrom> input);
    }

}