using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Argos.HtmlParser
{

    public sealed class HtmlOpenTagLexem : HtmlTagLexem
    {
        public HtmlOpenTagLexem(string name, string attrs = "") : base(name)
        {
            Attributes = attrs;
        }

        public string Attributes { get; }
    }

}