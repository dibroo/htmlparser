using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Argos.HtmlParser
{

    public sealed class HtmlRawTagLexem : HtmlLexem
    {
        static Regex _openTag = new Regex(@"<\s*(\w+)\s*(.*)>", RegexOptions.Compiled);
        static Regex _closeTag = new Regex(@"<\/\s*(\w+)\s*>", RegexOptions.Compiled);

        public HtmlRawTagLexem(string content)
        {
            Content = content;
        }

        public string Content { get; }

        public HtmlTagLexem Parse()
        {
            var match = _openTag.Match(Content);

            if (match.Success)
            {
                var name = match.Groups[1].Value;
                var attrs = match.Groups[2].Success ? match.Groups[2].Value : "";
                return new HtmlOpenTagLexem(name, attrs);
            }

            return new HtmlCloseTagLexem(_closeTag.Match(Content).Groups[1].Value);
        }
    }

}