using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Argos.HtmlParser
{

    public sealed class HtmlTagNode : HtmlNode
    {
        public HtmlTagNode(string name, string attrs, IEnumerable<HtmlNode> children)
        {
            Name = name;
            Attrs = attrs;
            Children = children;
        }

        public string Name { get; }
        public string Attrs { get; }
        public IEnumerable<HtmlNode> Children { get; }
    }

}