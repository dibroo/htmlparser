using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Argos.HtmlParser
{

    public sealed class HtmlTextNode : HtmlNode
    {
        public HtmlTextNode(string text)
        {
            Text = text;
        }

        public string Text { get; }
    }

}