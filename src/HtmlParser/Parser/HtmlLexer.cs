using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Argos.HtmlParser
{

    public sealed class HtmlLexer : ILexer<HtmlLexem>
    {
        public IEnumerable<HtmlLexem> Translate(IEnumerable<char> input)
        {
            var isTag = false;
            var builder = new StringBuilder();

            foreach(var c in input)
            {
                if (isTag)
                {
                    if (c == '<')
                        throw new Exception();
                    builder.Append(c);
                    if (c == '>')
                    {
                        isTag = false;
                        yield return new HtmlRawTagLexem(builder.ToString());
                        builder.Clear();
                    }
                }
                else
                {
                    if (c == '>')
                        throw new Exception();
                    if (c == '<')
                    {
                        isTag = true;
                        if (builder.Length != 0)
                            yield return new HtmlTextLexem(builder.ToString());
                        builder.Clear();
                    }
                    builder.Append(c);
                }
            }
            if (isTag) yield return new HtmlRawTagLexem(builder.ToString());
            else yield return new HtmlTextLexem(builder.ToString());
        }
    }

}