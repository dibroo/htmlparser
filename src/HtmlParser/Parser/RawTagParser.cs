using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Argos.HtmlParser
{

    public sealed class RawTagParser : ITranslator<HtmlLexem, HtmlLexem>
    {
        public IEnumerable<HtmlLexem> Translate(IEnumerable<HtmlLexem> input)
        {
            foreach(var l in input)
            {
                if (l is HtmlRawTagLexem)
                    yield return (l as HtmlRawTagLexem).Parse();
                else yield return l;
            }
        }
    }

}