using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Argos.HtmlParser
{

    public sealed class TreeConstructor : ITranslator<HtmlLexem, HtmlNode>
    {
        static HtmlNode Convert(object lexem)
        {
            if (lexem is HtmlNode)
                return lexem as HtmlNode;
            if (lexem is HtmlCloseTagLexem)
                return new HtmlTagNode((lexem as HtmlTagLexem).Name, "", Enumerable.Empty<HtmlNode>());
            if (lexem is HtmlOpenTagLexem)
                return new HtmlTagNode((lexem as HtmlTagLexem).Name, "", Enumerable.Empty<HtmlNode>());
            if (lexem is HtmlTextLexem)
                return new HtmlTextNode((lexem as HtmlTextLexem).Text);
            throw new ArgumentOutOfRangeException(nameof(lexem));
        }

        // TODO: Проверка тегов вида <br />
        static HtmlNode Unfold(HtmlCloseTagLexem lexem, LinkedList<object> acs)
        {
            var current = acs.First;
            var viewed = new LinkedList<object>();

            while (!(current.Value is HtmlOpenTagLexem 
                && (current.Value as HtmlOpenTagLexem).Name == lexem.Name))
            {
                viewed.AddFirst(current.Value);
                acs.RemoveFirst();
                current = acs.First;
            }
            acs.RemoveFirst();

            //if(viewed.Count > 1)
            //    viewed.RemoveLast();

            var children = viewed.Select(Convert).ToArray();

            return new HtmlTagNode(lexem.Name, (current.Value as HtmlOpenTagLexem).Attributes, children); 
        }

        public IEnumerable<HtmlNode> Translate(IEnumerable<HtmlLexem> input)
        {
            var acsendants = new LinkedList<object>();

            foreach(var i in input)
            {
                if (i is HtmlOpenTagLexem)
                {
                    acsendants.AddFirst(i);
                    continue;
                }
                if (i is HtmlCloseTagLexem)
                {
                    acsendants.AddFirst(Unfold(i as HtmlCloseTagLexem, acsendants));
                    continue;
                }
                if (i is HtmlTextLexem)
                {
                    acsendants.AddFirst(i);
                    continue;
                }
            }

            foreach (var i in acsendants)
                if (i is HtmlNode)
                    yield return i as HtmlNode;
        }
    }

}