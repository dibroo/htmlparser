using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Argos.HtmlParser
{

    public static class Utils
    {
        private class _ComposeTranslator<T1, T2, T3> : ITranslator<T1, T3>
        {
            private ITranslator<T1, T2> _t12;
            private ITranslator<T2, T3> _t23;

            public _ComposeTranslator(ITranslator<T1, T2> t12, ITranslator<T2, T3> t23)
            {
                _t12 = t12;
                _t23 = t23;
            }

            public IEnumerable<T3> Translate(IEnumerable<T1> input) 
                => _t23.Translate(_t12.Translate(input));
        }

        public static ITranslator<T1, T3> Compose<T1, T2, T3>
            (this ITranslator<T1, T2> t12, ITranslator<T2, T3> t23) 
            => new _ComposeTranslator<T1, T2, T3>(t12, t23);
    }

}