﻿using System;
using System.Linq;
using Xunit;
using Argos.HtmlParser;

namespace Tests
{
    public class TestsLexer
    {
        HtmlLexer lexer = new HtmlLexer();

        [Theory]
        [InlineData("text"), InlineData("sometext"), InlineData(":::!111")]
        public void LexerTest1(string s) 
        {
            var t = lexer.Translate(s);
            Assert.IsType<HtmlTextLexem>(t.First());
        }

        [Theory]
        [InlineData("<abc>"), InlineData("</text>")]
        public void LexerTest2(string s)
        {
            var tag = lexer.Translate(s);
            Assert.IsType<HtmlRawTagLexem>(tag.First());
        }

        [Fact]
        public void LexerTest3()
        {
            var tags = lexer.Translate("<abc>Text</abc>").ToArray();
            Assert.IsType<HtmlRawTagLexem>(tags[0]);
            Assert.IsType<HtmlTextLexem>(tags[1]);
            Assert.IsType<HtmlRawTagLexem>(tags[2]);
        }
    }

    public class TestsLexer2
    {
        static TestsLexer2()
        {
            lex = lexer.Compose(tagParser);
        }

        static HtmlLexer lexer = new HtmlLexer();
        static RawTagParser tagParser = new RawTagParser();

        static ITranslator<char, HtmlLexem> lex;

        [Theory]
        [InlineData("<abc>"), InlineData("<text text=text>")]
        public void LexerTest1(string s)
        {
            var p = lex.Translate(s);

            Assert.IsType<HtmlOpenTagLexem>(p.First());
        }

        [Theory]
        [InlineData("</abc>"), InlineData("</text>")]
        public void LexerTest3(string s)
        {
            var p = lex.Translate(s);

            Assert.IsType<HtmlCloseTagLexem>(p.First());
        }

        [Fact]
        public void LexerTest2()
        {
            var p = lex.Translate("<text t=t>").First() as HtmlOpenTagLexem;

            Assert.Equal("text", p.Name);
            Assert.Equal("t=t", p.Attributes);
        }
    }

    public class TestParser
    {
        static TestParser()
        {
            parser = lexer.Compose(tagParser).Compose(constr);
        }

        static HtmlLexer lexer = new HtmlLexer();
        static RawTagParser tagParser = new RawTagParser();
        static TreeConstructor constr = new TreeConstructor();
        static ITranslator<char, HtmlNode> parser;
    }
}
